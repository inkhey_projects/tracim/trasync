# Trasync

![pipeline_build_status](https://framagit.org/inkhey/trasc/badges/master/pipeline.svg)
![PyPI - License](https://img.shields.io/pypi/l/trasync)
![PyPI - Status](https://img.shields.io/pypi/status/trasync)
[![PyPI](https://img.shields.io/pypi/v/trasync) ![PyPI - Python Version](https://img.shields.io/pypi/pyversions/trasync) ![PyPI](https://img.shields.io/pypi/dm/trasync)](https://pypi.org/project/trasync/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![mypy](https://img.shields.io/badge/mypy-checked-blueviolet)

An Async http api for Tracim

## Install

### From pypi

```sh
$ pip install trasync
```

### From source

- clone this repository
- `pip install -e "."`

## Usage
